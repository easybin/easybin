import { Component, Input, Output, EventEmitter, OnInit, OnChanges} from '@angular/core';
import * as _ from 'lodash';

declare const $;

@Component({
  selector: 'app-load-data-modal',
  templateUrl: './load-data-modal.component.html',
  styleUrls: ['./load-data-modal.component.css']
})
export class LoadDataModalComponent implements OnInit {
  @Input() id: string;
  @Output() file: EventEmitter<any> = new EventEmitter();

  private helpStep: number = 1;
  private files: any = null;
  private stepId: string = '';
  private index: string = '';

  constructor() {
  }

  ngOnInit() {}

  openHelpModal(files,stepId,index) {
    this.files = files.files;
    this.stepId = stepId;
    this.index = index;
    this.helpStep = 1;
    $(`[modal='load-data'][id=${this.id}]`).modal('show');
  }


  loadfile(event) {
    // event.stopPropagation();
    this.file.emit({ file: event.filepath, stepId: this.stepId, index: this.index});
    $("[modal='load-data']").modal('hide');
  }

  prevHelpModalStep() {
    const stepVal = this.helpStep - 1;

    this.helpStep = _.max([stepVal, 1]);
  }

  nextHelpModalStep() {
    const stepVal = this.helpStep + 1;

    this.helpStep = _.min([stepVal, 3]);
  }

}
