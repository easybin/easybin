import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadDataModalComponent } from './load-data-modal.component';

describe('HomePageHelpModalComponent', () => {
  let component: LoadDataModalComponent;
  let fixture: ComponentFixture<LoadDataModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadDataModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadDataModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
