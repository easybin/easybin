import { Component, OnInit } from '@angular/core';
import { ExecuteFlowService } from '../../../services/work-flow/execute-flow.service';


declare const $;

@Component({
  selector: 'app-test-data-modal',
  templateUrl: './test-data-modal.component.html',
  styleUrls: ['./test-data-modal.component.css'],
  providers: [
    ExecuteFlowService
  ]
})
export class TestDataModalComponent implements OnInit {

  file: any = null;
  public uploading:boolean = false;
  public uploaded:boolean = false;

  constructor(private executionService: ExecuteFlowService){}

  ngOnInit() {
    this.file = null;
  }

  openHelpModal() {
    $("[modal='home-page-test-data']").modal('show');
  }

  clickSelectFile() {
    $(`#fileUploadField`).click();
    this.clear();
  }

  upload(){
    this.uploading = true;
    console.log(this.uploading)
    this.executionService.upload(this.file).subscribe((results) => {
      console.log("uploaded file : "+ results.status);
      this.uploading = false;
      this.uploaded = true;
    });
  }

  clear(){
    this.uploading = false;
    this.uploaded = false;
  }

//   upload_s(file: any): Observable<any> {
//     this.progress = 1;

//     // const headers: Headers = new Headers();
//     const formData = this.executionService.createFormData({ file });

//     for (let [key, value] of formData.entries()) {
//       console.log(key, value);
//     }

//     return this.http.post(
//       `${config.apiUrl}/api/upload`, formData,{
//         reportProgress: true,
//         observe: "events"
//       }
//     ).pipe(
//       map((event: any) => {
//       if (event.type == HttpEventType.UploadProgress) {
//         this.progress = Math.round((100 / event.total) * event.loaded);
//       } else if (event.type == HttpEventType.Response) {
//         this.progress = null;
//       }
//     })
//    );
//   }
 }
