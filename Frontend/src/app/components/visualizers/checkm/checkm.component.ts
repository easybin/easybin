import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from '../../modals/modal/modal.component';
import * as _ from 'lodash';
import { ExecuteFlowService } from '../../../services/work-flow/execute-flow.service';
import { DomSanitizer } from '@angular/platform-browser';


declare var require: any;
declare const $;

@Component({
  selector: 'app-checkm-msa',
  templateUrl: './checkm.component.html',
  styleUrls: ['./checkm.component.css'],
  providers: [
    ExecuteFlowService
  ]
})
export class checkmComponent implements OnInit {
  @ViewChild('checkmViewRaw') modal: ModalComponent;
  private head: string = null;


  private imageObjects: any = [];
  private imagePaths: any = null;


  private index: number = 0;

  private image: any = null;
  active: boolean = true;

  constructor(private executionService: ExecuteFlowService, private sanitizer: DomSanitizer) {

  }

  getThisimage(path) {
    this.executionService.getimage(path).subscribe((result) => {
      let objectURL = URL.createObjectURL(result);
      this.image = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    })
  }

  ngOnInit() {
  }

  render(data) {
    console.log("rendering");
    this.head = data.name;
    this.index = 0;

    this.imagePaths = data.display;
    console.log(this.imagePaths);
    this.getThisimage(this.imagePaths[this.index]);
    this.modal.openModal();
  }

  clear() {
    document.getElementById('checkm').innerHTML = '';
  }

  viewRaw() {
    this.modal.openModal();
  }

  prevHelpModalStep() {
    if( 0 <= this.index - 1){
      this.index = this.index - 1;
    }else{
      this.index = this.imagePaths.length - 1;
    }
    this.active = false;
  }

  nextHelpModalStep() {   
    if(this.index + 1 < this.imagePaths.length){
      this.index = this.index + 1;
    }else{
      this.index = 0;
    }
    this.active = false;
  }

  onTransitionEnd() {
    this.active = true;
    this.getThisimage(this.imagePaths[this.index]);    
    
  }
}
