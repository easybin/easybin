import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { checkmComponent } from './checkm.component';

describe('checkmComponent', () => {
  let component: checkmComponent;
  let fixture: ComponentFixture<checkmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ checkmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(checkmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
