import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { LoadDataModalComponent } from '../modals/load-data-model /load-data-modal.component';
import { ExecuteFlowService } from '../../services/work-flow/execute-flow.service';
import { checkmComponent } from '../visualizers/checkm/checkm.component';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs';

import * as _ from 'lodash';
declare const jsPlumb: any;
declare const $: any;
// declare var angular: any;

@Component({
  selector: 'app-step-box',
  templateUrl: './step-box.component.html',
  styleUrls: ['./step-box.component.css'],
  providers: [
    LoadDataModalComponent,
    ExecuteFlowService,
    checkmComponent
  ]
})
export class StepBoxComponent implements OnInit {
  @Input() step: any;
  @Input() activeWorkflow: any;
  @Output() boxClick: EventEmitter<any> = new EventEmitter();
  @Output() remove: EventEmitter<any> = new EventEmitter();

  private images: any = [];

  @ViewChild(LoadDataModalComponent) loadDataModal: LoadDataModalComponent;
  @ViewChild(checkmComponent) checkmView: checkmComponent;

  constructor(private executionService: ExecuteFlowService, private sanitizer: DomSanitizer) { }

  opendataModal(stepId, i) {
    this.executionService.getfiles().subscribe((results) => {
      this.loadDataModal.openHelpModal(results, stepId, i);
    })
  }

  openVisualizer(name, display) {
    this.checkmView.render({ name: name, display: display});
  }

  upload(object) {
    document.getElementById(`fileFieldname_${object.stepId}_${object.index}`).setAttribute("value", object.file.replace(/.*[\/\\]/, ''));
    this.step.inputs[object.index]['value'] = object.file;
  }

  clickSelectFile(stepId, input) {
    $(`#fileUploadField_${stepId}_${input}`).click();
  }

  handleUpload(e, stepId, input): void {
    console.log("files : " + e.target.value.replace(/.*[\/\\]/, ''));
    document.getElementById(`fileFieldname_${stepId}_${input}`).setAttribute("value", e.target.value.replace(/.*[\/\\]/, ''));
  }

  ngOnInit() {
    // $(".item").resizable({
    //   resize : function(event, ui) {
    //     jsPlumb.repaint(ui.helper);
    //   },
    //   handles: "all"
    // });
  }

  onBoxClick(event) {
    event.stopPropagation();
    this.boxClick.emit(this.step);
  }

  onInputPress(event, id) {
    event.stopPropagation();
    $(`#${id}`).dialog('open');
  }

  onDialogCancel(input, id) {
    input.tempValue = input.value;
    $(`#${id}`).dialog('close');
  }

  onDialogOk(input, id) {
    console.log(input);
    //$(`#${id}`).dialog('close');
  }

  onStepRemove(event) {
    event.stopPropagation();
    this.remove.emit(this.step);
  }

  ongetdata() {
    console.log(this.step);
  }

}
