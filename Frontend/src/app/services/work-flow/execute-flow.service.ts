import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/toPromise';
import * as config from '../../configs/index';

@Injectable()
export class ExecuteFlowService {

  private websocket: any;

  constructor(private http: HttpClient) {
  }

  // takes a {} object and returns a FormData object
  createFormData(object: Object, form?: FormData, namespace?: string): FormData {
    const formData = form || new FormData();
    for (let property in object) {
      if (!object.hasOwnProperty(property) || !object[property]) {
        continue;
      }
      const formKey = namespace ? `${namespace}[${property}]` : property;

      if (object[property] instanceof Date) {
        formData.append(formKey, object[property].toISOString());

      } else if (typeof object[property] === 'object' && !(object[property] instanceof File)) {
        this.createFormData(object[property], formData, formKey);

      } else {
        formData.append(formKey, object[property]);
      }
    }
    return formData;
  }

  upload(file: any): Observable<any> {

    // const headers: Headers = new Headers();
    const formData = this.createFormData({ file });

    for (let [key, value] of formData.entries()) {
      console.log(key, value);
    }

    return this.http.post(
      `${config.apiUrl}/api/upload`, formData
    );
  }

  getfiles(): Observable<any> {
    return this.http.get(
      `${config.apiUrl}/api/files`
    );
  }

  getimage(path): Observable<any> {
    return this.http.get(
      `${config.apiUrl}/api/source?path=${path}`
      , { responseType: 'blob' });
  }

  // getimages(paths): Observable<any[]> {
  //   let images = [];
  //   for (let path in paths) {
  //     images.push(this.http.get(`${config.apiUrl}/api/source?path=${path}`,{ responseType: 'blob' }))
  //   }
  //   console.log("if we get all the images once "+images.length);
    
    
  //   // return Observable.create(observer => observer.next(images));
  //   return Observable.from(images);
  // }


  executeFlow(workflow: any): Observable<any> {
    this.websocket = new WebSocket(`${config.socketUrl}/api/flows`);
    this.websocket.onopen = (evt) => {
      this.websocket.send(JSON.stringify({ workflow: workflow }));
    };

    return Observable.create(observer => {
      this.websocket.onmessage = (evt) => {
        observer.next(evt);
      };
    }).map(res => res.data).share();
  }

  public close() {
    console.log('on closing WS');
    this.websocket.close();
  }

  // executeFlow(workflow: any): Observable<any> {

  //   // const headers: Headers = new Headers();
  //   const formData = this.createFormData({ workflow: workflow });

  //   for (let [key, value] of formData.entries()) {
  //     console.log(key, value);
  //   }

  //   return this.http.post(
  //     `${config.apiUrl}/api/flow`, formData
  //   );
  // }

  getStatus(): Observable<any> {
    return this.http.get(
      `${config.apiUrl}/api/status`
    );
  }

  // const fetchURL = 'http://192.168.22.124:3000/source/';
  // const image = name.map((picName) => {
  //   return picName
  // })

  // fetch(fetchURL + this.image)
  // .then(response => response.json())
  // .then(images => console.log(fetchURL + images));

}

