
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { WebSocketService} from './WebSocketService';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/toPromise';
import * as config from '../../configs/index';

//  const subject = Observable.webSocket('ws://localhost:3000/api/flows');
@Injectable()
export class WebSocketTestService {
    public messages: Subject<any>  = new Subject<any>();

    public data: MessageEvent  = new MessageEvent('message'); 

    constructor(private wsService: WebSocketService) {
        console.log('constructyor ws synop service')
    }

    public connect() {
        this.messages = <Subject<any>>this.wsService
            .connect('http://18.205.30.68/api/flows')
            .map((response: any): any => {
                return JSON.parse(response.data);
            })

            this.wsService.connect('ws://18.205.30.68/api/flows').next(this.data);
    }

    public close() {
        this.wsService.close()
    }
} // end class 