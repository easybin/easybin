import { Component, OnInit } from '@angular/core';
import { TreeViewService } from '../../services/support/tree-view.service';


@Component({
  selector: 'app-forms-page',
  templateUrl: './forms-page.component.html',
  styleUrls: ['./forms-page.component.css'],
  providers: [
    TreeViewService
  ]
})
export class FormsPageComponent implements OnInit {
  // localStorage = localStorage;

  private resultData: any = null;
  private workflowId: string;

  constructor(private ViewService: TreeViewService) { }

  ngOnInit() {
  }

  search() {
    this.ViewService.getWorkflow(this.workflowId).subscribe((results) => {
      this.resultData = results.result;
      console.log('results came ', results.result);
    });
  }

}


