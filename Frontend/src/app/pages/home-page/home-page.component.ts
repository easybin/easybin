import { Component, OnInit, ViewChild } from '@angular/core';
import { TreeViewComponent } from '../../components/tree-view/tree-view.component';
import { DrawingBoardComponent } from '../../components/drawing-board/drawing-board.component';
import { ExecuteFlowService } from '../../services/work-flow/execute-flow.service';

import { ChangeDetectionStrategy, ChangeDetectorRef, Input } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import { map, distinctUntilChanged } from 'rxjs/operators';
// import { ConfirmationDialogService } from '../../components/modals/confirmation-dialog/confirmation-dialog.service';

// import { PairwiseBlastComponent } from '../../components/visualizers/pairwise-blast/pairwise-blast.component';
// import { ClustalOmegaMsaComponent } from '../../components/visualizers/clustal-omega-msa/clustal-omega-msa.component';
// import { DialignMsaComponent } from '../../components/visualizers/dialign-msa/dialign-msa.component';
// import { TCoffeeMsaComponent } from '../../components/visualizers/t-coffee-msa/t-coffee-msa.component';

import * as _ from 'lodash';
import uuidv4 from 'uuid/v4';
import { IfObservable } from 'rxjs/observable/IfObservable';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
  providers: [
    TreeViewComponent,
    DrawingBoardComponent,
    ExecuteFlowService,
    // ConfirmationDialogService
    // PairwiseBlastComponent
  ],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomePageComponent implements OnInit {
  @ViewChild(DrawingBoardComponent) drawingBoard: DrawingBoardComponent;
  // @ViewChild(MomentComponent) momentComponent: MomentComponent;
  // @ViewChild(PairwiseBlastComponent) pairwiseBlasterView: PairwiseBlastComponent;
 
  // @ViewChild(DialignMsaComponent) dialignView: DialignMsaComponent;
  // @ViewChild(TCoffeeMsaComponent) tCoffeeView: TCoffeeMsaComponent;

  private start: moment.Moment;
  timeFromNow: Observable<string>;


  private activeTreeItem: any = null;
  private readyWorkflow: any = null;
  private activeWorkflow: any = null;
  private finishedWorkflow: any = null;
  private resultData: any = null;
  private statusData: any;
  private workflowId = null;
  private end = null;
  private timeSpan = null;
  
  constructor(private executionService: ExecuteFlowService) { }

  ngOnInit() {
    this.readyWorkflow = false;
    this.activeWorkflow = false;
    this.finishedWorkflow = false;
  }

  // openConfirmationDialog() {
  //   this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to ... ?')
  //   .then((confirmed) => console.log('User confirmed:', confirmed))
  //   .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  // }

  selectTree(item) {
    this.activeTreeItem = item;
  }

  clickAddStep(item) {
    if (this.activeWorkflow) {
      if (confirm("You already have a workflow running. Are you sure you want to start a new workflow?")) {
        this.addStep(item);
      }
    } else {
      this.addStep(item);
    }
  }

  addStep(item) {
    this.activeWorkflow = false;
    this.drawingBoard.offActive();
    this.activeTreeItem = item;
    (this.activeTreeItem.isStep) && this.drawingBoard.addStep(this.activeTreeItem);
    this.readyWorkflow = true;
  }

  offReadyworkflow() {
    this.readyWorkflow = false;
  }

  startTimer() {
    this.start = moment(new Date());
    this.timeFromNow = Observable.interval(1000).pipe(
      map(() => this.start.fromNow()),
      distinctUntilChanged()
    ).takeWhile(() => this.end == null);
  }

  execute() {

    this.end = null;
    this.timeSpan = null;
    this.finishedWorkflow = false;
    this.workflowId = uuidv4();
    const sequence = this.drawingBoard.getStepSequence(this.workflowId);

    if (sequence.length > 0) {
      const workflow = {
        workflowId: this.workflowId,
        sequence: sequence
      }

      alert("Your workflowID : " + workflow.workflowId);
      this.activeWorkflow = true;
      this.drawingBoard.setActive();
      this.startTimer();

      this.executionService.executeFlow(workflow).subscribe((results) => {
        this.resultData = JSON.parse(results);
        // console.log(this.resultData);

        if (this.resultData.messageType == "status") {
          for (let item of this.resultData.steps) {
            if (item.computedValue) {
              if(typeof item.computedValue.output.images!== 'undefined' && item.computedValue.output.images.length>0){
                console.log("have images ");
                this.drawingBoard.addStatus(item.stepId, this.resultData.status, item.computedValue.output.bins, item.computedValue.output.images);
              }else{
                console.log("don't have images ");
                this.drawingBoard.addStatus(item.stepId, this.resultData.status, item.computedValue.output.bins, []);
              }
              // this.checkmView.render(item.computedValue.output.images);
            } else {
              this.drawingBoard.addStatus(item.stepId, this.resultData.status, null, []);
            }
          }
        } else if (this.resultData.messageType == "output") {
          this.executionService.close();
          this.end = moment(new Date());
          this.timeSpan = moment.utc(moment(this.end,"DD/MM/YYYY HH:mm:ss").diff(moment(this.start,"DD/MM/YYYY HH:mm:ss"))).format("HH:mm:ss")
        }
        this.activeWorkflow = false;
        this.drawingBoard.offActive();
        this.finishedWorkflow = true;

        // alert("WorkflowID : " + results.workflow + "is completed");

      });
    }
  }

  clickExecute() {
    // this.clearUI();
    if (this.activeWorkflow) {
      if (confirm("You already have a workflow running. Are you sure you want to execute a new workflow?")) {
        this.execute();
      }
    } else {
      this.execute();
    }

  }

  // clearUI() {
  //   this.pairwiseBlasterView.clear();
  //   this.clustalOmegaView.clear();
  //   this.dialignView.clear();
  //   this.tCoffeeView.clear();
  // }

  // off() {
  //   document.getElementById("overlay").style.display = "none";
  // }

}
