import cmd from 'node-cmd';
import fs from 'fs';
import cp from 'child_process';
import path from 'path';
import * as _ from 'lodash';

export default class WrapCheckM {
    workflowId;
    stepId;

    // TODO these are the parameters
    constructor(workflowId = null, stepId = null, inp = null) {
        this.workflowId = workflowId;
        this.stepId = stepId;
        this.input = inp[0];
        // console.log(this.input);

        // const sample_inp =
        // {
        //     step: 'ConCoct',
        //     output:
        //     {
        //         bins:
        //             '/home/sandani/FYP/SandBox/EayBin-backend/results/a1d52e29-e248-4068-8460-10a180fe7dc9/397d76ff-7f5a-499d-920b-d527d3ad1b94/concoct_output/concoct_bins',
        //         bin_type: 'fa'
        //     }
        // };
        // this.input = sample_inp;
    }

    exec() {

        return new Promise((resolve, reject) => {
            try {
                console.log('Running checkM Tool...', this.stepId);
                console.time(`Running time : ${this.stepId}`);

                // var str = ``;

                // for (let param of this.input) {
                //     str = str + `ln -s ${param.output.bins} ${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/input\n`
                // }
                // console.log(this.input);

                const command = `checkm gc_plot -x ${this.input.output.bin_type} ${this.input.output.bins} visualise_output 95`;


                // const command = `checkm gc_plot -x fa ${this.input.output.bins} visualise_output 95\ncheckm lineage_wf -t 8 -x fa ${this.input.output.bins} output`;
                // console.log(command);

                fs.mkdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}`, () => {
                    // fs.mkdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/input`, () => {
                    cp.exec(command, { cwd: process.chdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}`) }, (err, data, stderr) => {
                        if (err == null || stderr == null) {
                            const bin_files = `${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/visualise_output/`;
                            const images = []
                            // var i = 1;
                            // while (i <= 2) {
                            //     images.push(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/visualise_output/Sample_${i}.gc_plots`);
                            //     i++;
                            // }

                            fs.readdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/visualise_output/`, (err, filenames) => {
                                _.each(filenames, (file) => {
                                    if (path.extname(file) === `.png`) {
                                        console.log(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/visualise_output/${file}`);
                                        images.push(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/visualise_output/${file}`);
                                    }
                                });
                                // return Promise object 
                                resolve({ step: 'checkM', output: { bins: bin_files, images: images } });
                                console.timeEnd(`Running time : ${this.stepId}`);

                            });

                        } else {
                            reject("CheckM Tool execution failed\n" + err);

                        }

                    });
                });
                // });
                // process.chdir('../../../../');
            }
            catch (err) {
                console.error(`CheckM error : ${err}`);
            }
        });
    }
}