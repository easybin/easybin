import cmd from 'node-cmd';
import cp from 'child_process';
import fs from 'fs';
import path from 'path';

export default class WrapAbundanceBin {
    workflowId;
    stepId;
    inputFilename;
    kmer_len;
    exclude;
    exclude_max;
    bin_num;
    method;

    // TODO these are the parameters
    constructor(workflowId = null, stepId = null, inp = null) {
        this.workflowId = workflowId;
        this.stepId = stepId;
        for (let param of inp) {
            switch (param.name) {
                case 'Sequence':
                    this.inputFilename = param.value;
                    break;
                case 'bin num':
                    this.bin_num = param.value;
                    break;
                case 'exclude':
                    this.exclude = param.value;
                    break;
                case 'exclude_max':
                    this.exclude_max = param.value;
                    break;
                case 'method':
                    this.method = param.value;
                    break;
                case 'kmer_len':
                    this.kmer_len = param.value;
                    break;
            }
        }
    }

    exec() {
        //if bin number is given then it should be used to bin, else -RECURSIVE_CLASSIFICATION should be used

        return new Promise((resolve, reject) => {
            try {
                console.log(`Running AbundanceBin Tool... ${this.stepId} : processID : ${process.pid}`);
                console.time(`Running time : ${this.stepId}`);
                const command = `${process.env.ROOT_DIR}/python-wrappers/abundanceBin/abundancebin -input ${this.inputFilename} -output ${path.basename(this.inputFilename)} -bin_num 2`;
               
                fs.mkdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}`, ()=>{
                    cp.exec( command, { cwd: process.chdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}`) }, (err, data, stderr) => {

                        if (err == null || stderr == null) {
                            const bin_files = `${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}`;
    
                            //return Promise object 
                            resolve({ step: 'Abundancebin', output: { bins: bin_files, bin_type: 'fasta'} });
                            console.timeEnd(`Running time : ${this.stepId}`);
                        } else {
                            reject("Abundancebin Tool execution failed\n" + err);
                        }    
                    });
                });
            }
            catch (err) {
                console.error(`Abundancebin Error :  ${err}`);
            }
        });
    }
}
