import fs from 'fs';
import path from 'path';
import cp from 'child_process';

export default class WrapMaxbin {
    workflowId;
    stepId;
    inputContigFilename;
    inputReadFilename1;
    inputReadFilename2;

    // TODO these are the parameters
    constructor(workflowId = null, stepId = null, inp = null) {
        this.workflowId = workflowId;
        this.stepId = stepId;
        for (let param of inp) {
            switch (param.name) {
                case 'ContigFile':
                    this.inputContigFilename = param.value;
                    break;
                case 'ReadFile_1':
                    this.inputReadFilename1 = param.value;
                    break;
                case 'ReadFile_2':
                    this.inputReadFilename2 = param.value;
                    break;
            }
        }
    }

    exec() {

        return new Promise((resolve, reject) => {
            try {
                console.log(`Running Maxbin Tool... ${this.stepId} : ProcessID : ${process.pid}`);
                console.time(`Running time : ${this.stepId}`);

                fs.mkdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}`, async () => {
                    let child_process_obj = cp.exec(`run_MaxBin.pl -contig ${this.inputContigFilename} -reads ${this.inputReadFilename1} -reads2 ${this.inputReadFilename2} -out myout`, { cwd: process.chdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}`) }, (err, data, stderr) => {

                        if (err == null || stderr == null) {

                            fs.mkdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/maxBin_bins/`, () => {
                                fs.readdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}`, async (err, filenames) => {

                                    var listOfPromises = filenames.map(renameFiles);
                                    await Promise.all(listOfPromises);

                                    console.log("Files moved");
                                    const bin_files = `${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/maxBin_bins/`;
                                    resolve({ step: 'Maxbin', output: { bins: bin_files, bin_type: 'fasta' } });
                                    console.timeEnd(`Running time : ${this.stepId}`);
                                });
                            });

                            const renameFiles = file => {
                                if (path.extname(file) === `.fasta`) {
                                    fs.rename(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/${file}`, `${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/maxBin_bins/${file}`, () => {
                                        return new Promise(resolve => { resolve() });
                                    });
                                }
                            };
                        } else {
                            reject("Maxbin Tool execution failed\n" + err);
                        }
                    });

                    child_process_obj.on('exit', function(code){
                        console.log(`MaxBin child process exited with code : ${code}`);

                    });
                    console.log(`MaxBin Launched child process: PID: ${child_process_obj.pid}`);
                });
            }
            catch (err) {
                console.error(`Maxbin error : ${err}`);
            }
        });
    }
}