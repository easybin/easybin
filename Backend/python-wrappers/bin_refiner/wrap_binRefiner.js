import cmd from 'node-cmd';
import fs from 'fs';
import path from 'path';
import cp from 'child_process';
export default class WrapBinRefiner {
    workflowId;
    stepId;

    // TODO these are the parameters
    constructor(workflowId = null, stepId = null, inp = null) {
        this.workflowId = workflowId;
        this.stepId = stepId;
        this.input = inp;
        // console.log(this.input);

        // console.log("**********************************");

        // const sample_inp = [
        //     {
        //       step: 'ConCoct',
        //       output: {
        //         bins: '/home/sandani/FYP/SandBox/EayBin-backend/results/92963b14-b1a1-4fee-bff4-720c56750a1f/c9270c63-7b28-48a3-af62-3e8b3655f348/concoct_output/concoct_bins/',
        //         bin_type: 'fa'
        //       }
        //     },
        //     {
        //       step: 'Maxbin',
        //       output: {
        //         bins: '/home/sandani/FYP/SandBox/EayBin-backend/results/92963b14-b1a1-4fee-bff4-720c56750a1f/525869ac-2738-49fa-ae7a-e4e1bcfc1a2d/maxBin_bins/',
        //         bin_type: 'fasta'
        //       }
        //     }
        //   ];
          
        // this.input = sample_inp;
    }

    exec() {

        return new Promise((resolve, reject) => {
            // const command = `
            // ln -s /home/nethma/FYP/easybin/EayBin-backend/python-wrappers/maxBin/bins/maxBin_output/maxBin/ input_bin_folder/
            // ln -s /home/nethma/FYP/easybin/EayBin-backend/python-wrappers/concoct/bins/concoct_output/concoct/ input_bin_folder/
            // Binning_refiner -i input_bin_folder -p Sample -plot
            // `;

            try {
                console.log('Running BinRefiner Tool...', this.stepId);
                console.time(`Running time : ${this.stepId}`);
                // process.chdir('python-wrappers/bin_refiner/');

                var str = ``;

                for (let param of this.input) {
                    str = str + `ln -s ${param.output.bins} ${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/input_bin_folder\n`
                }

                const command = `${str}` + `Binning_refiner -i ${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/input_bin_folder -p Sample -plot`;
                // console.log(command);

                fs.mkdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}`, () => {
                    fs.mkdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/input_bin_folder`, () => {
                        let child_process_obj = cp.exec(command, { cwd: process.chdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}`) }, (err, data, stderr) => {

                            if (err == null || stderr == null) {
                                const bin_files = `${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/Sample_Binning_refiner_outputs/Sample_refined_bins/`;

                                //return Promise object 
                                resolve({ step: 'BinRefiner', output: { bins: bin_files, bin_type: 'fasta' } });
                                console.timeEnd(`Running time : ${this.stepId}`);
                            } else {
                                reject("BinRefiner Tool execution failed\n" + err);
                            }
                        });
                        child_process_obj.on('exit', function(code){
                            console.log(`BinRef Launched child process: PID: ${child_process_obj.pid} and exited with code : ${code}`);
                        });
                    });
                });
            }
            catch (err) {
                console.error(`Bin refiner Error :  ${err}`);
            }
        });
    }



}
