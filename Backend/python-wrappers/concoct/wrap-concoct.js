import cmd from 'node-cmd';
import fs from 'fs';
import cp from 'child_process';

// const workerPool = require("workerPool");
// let pool = workerPool.pool();


export default class WrapConcoct {
    workflowId;
    stepId;
    inputContigFilename;
    inputReadFilename1;
    inputReadFilename2;

    // TODO these are the parameters
    constructor(workflowId = null, stepId = null, inp = null) {
        this.workflowId = workflowId;
        this.stepId = stepId;
        for (let param of inp) {
            switch (param.name) {
                case 'ContigFile':
                    this.inputContigFilename = param.value;
                    break;
                case 'ReadFile_1':
                    this.inputReadFilename1 = param.value;
                    break;
                case 'ReadFile_2':
                    this.inputReadFilename2 = param.value;
                    break;
            }
        }
    }


    exec() {
       
        return new Promise((resolve, reject) => {
            try {
                console.log(`Running ConCoct Tool... ${this.stepId} : ProcessID : ${process.pid}`);
                console.time(`Running time : ${this.stepId}`);

                fs.mkdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}`, () => {
                    let child_process_obj = cp.exec(`
                    ln -s ${this.inputReadFilename1} pair1.fa
                    ln -s ${this.inputReadFilename2}  pair2.fa
                    ln -s ${this.inputContigFilename} contigs.fa
                    bowtie2-build contigs.fa contigs.fa
                    bowtie2 -p 8 -x contigs.fa -1 pair1.fa -2 pair2.fa -S out.map.sam
                    samtools sort -o out.map.sorted.bam -O bam out.map.sam
                    samtools index out.map.sorted.bam
                    conda run -n concoct_env cut_up_fasta.py contigs.fa -c 10000 -o 0 --merge_last -b contigs_10K.bed > contigs_10K.fa
                    conda run -n concoct_env concoct_coverage_table.py contigs_10K.bed out.map.sorted.bam > coverage_table.tsv
                    conda run -n concoct_env concoct --composition_file contigs_10K.fa --coverage_file coverage_table.tsv -b concoct_output/
                    conda run -n concoct_env merge_cutup_clustering.py concoct_output/clustering_gt1000.csv > concoct_output/clustering_merged.csv
                    mkdir concoct_output/concoct_bins
                    conda run -n concoct_env extract_fasta_bins.py contigs.fa concoct_output/clustering_merged.csv --output_path concoct_output/concoct_bins
                    `, { cwd: process.chdir(`${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}`) },
                    (err, data, stderr) => {

                        // console.log(`New directory for Concoct: ${process.cwd()}`);

                        if (err == null || stderr == null) {    
                            const bin_files = `${process.env.ROOT_DIR}/results/${this.workflowId}/${this.stepId}/concoct_output/concoct_bins/`;
         
                            //return Promise object 
                            resolve({ step: 'ConCoct', output:  { bins: bin_files, bin_type: 'fa'}  });
                            console.timeEnd(`Running time : ${this.stepId}`);
                        } else {
                            reject("ConCoct Tool execution failed\n" + err);
                        }
                    }); 

                    child_process_obj.on('exit', function(code){
                        console.log(`COCT child process exited with code : ${code}`);

                    });
                    console.log(`COCT Launched child process: PID: ${child_process_obj.pid}`);
                })

                // process.chdir(`python-wrappers/concoct/bin_${this.workflowId}`);
                // console.log(`New directory: ${process.cwd()}`);

                //  const command = `
                //     ln -s ../../../uploads/${this.inputReadFilename1} pair1.fastq
                //     ln -s ../../../uploads/${this.inputReadFilename2}  pair2.fastq
                //     ln -s ../../../uploads/${this.inputContigFilename} contigs.fa
                //     bowtie2-build contigs.fa contigs.fa
                //     bowtie2 -p 8 -x contigs.fa -1 pair1.fastq -2 pair2.fastq -S out.map.sam
                //     samtools sort -o out.map.sorted.bam -O bam out.map.sam
                //     samtools index out.map.sorted.bam
                //     conda run -n concoct_env cut_up_fasta.py contigs.fa -c 10000 -o 0 --merge_last -b contigs_10K.bed > contigs_10K.fa
                //     conda run -n concoct_env concoct_coverage_table.py contigs_10K.bed out.map.sorted.bam > coverage_table.tsv
                //     conda run -n concoct_env concoct --composition_file contigs_10K.fa --coverage_file coverage_table.tsv -b concoct_output/
                //     conda run -n concoct_env merge_cutup_clustering.py concoct_output/clustering_gt1000.csv > concoct_output/clustering_merged.csv
                //     mkdir concoct_output/concoct_bins
                //     conda run -n concoct_env extract_fasta_bins.py contigs.fa concoct_output/clustering_merged.csv --output_path concoct_output/concoct_bins
                //     `;
            }
            catch (err) {
                console.error(`ConCoct error : ${err}`);
            }
        });
    }
}