export default class WrapVisualizer {
    workflowId;

    // TODO these are the parameters
    constructor(workflowId = null, inp = null) {
        console.log("workflowId in Tool-->", workflowId);
        this.workflowId = workflowId;
        this.input = inp;
    }

    exec(){
        console.log('Running visualizer Tool...');
        return new Promise(resolve => resolve({step: 'visualizer', output: 'visualizer Results'}));
    }
}