import {MongoClient}  from 'mongodb';

export default class DBHandler {

    static insertView(viewObject, callback) {
        MongoClient.connect(process.env.MONGO_URL, (err, client) => {
            const db = client.db(process.env.MONGO_DB_NAME);
            const collection = db.collection(process.env.MONGO_COLLECTION);

            collection.insertOne(viewObject, (err, result) => {
                callback(result);
            });
            client.close();
        });
    }

    static updateView( key ,viewObject, callback) {
        MongoClient.connect(process.env.MONGO_URL, (err, client) => {
            const db = client.db(process.env.MONGO_DB_NAME);
            const collection = db.collection(process.env.MONGO_COLLECTION);

            collection.updateOne(key, viewObject, {upsert : true}, (err, result) => {
                callback(result);
            });
            client.close();
        });
    }   

    static getAllViews(callback) {
        MongoClient.connect(process.env.MONGO_URL, (err, client) => {

            console.log("In db handler");
            const db = client.db(process.env.MONGO_DB_NAME);
            const collection = db.collection(process.env.MONGO_COLLECTION);

            collection.find({}).toArray((err, result) => {
                callback(result);
            });
            client.close();
        });
    }


    static getView(viewObject, callback) {
        MongoClient.connect(process.env.MONGO_URL, (err, client) => {

            console.log("In db handler");
            const db = client.db(process.env.MONGO_DB_NAME);
            const collection = db.collection(process.env.MONGO_COLLECTION);

            collection.findOne(viewObject, (err, result) => {
                callback(result);
            });
            client.close();
        });
    }
}