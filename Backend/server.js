require('dotenv').config();
import step_executor from './workflow/step-executor';
import DBHandler from './db/db-handler';
import Step from './workflow/step';
import logger from './logger';
const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;

if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);

    for (let i = 0; i < numCPUs; i++) {
        cluster.fork()
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`)
    })
} else {
    const workflowRouter = require('./routes/work-flow');
    const treeViewRouter = require('./routes/tree-view');

    const express = require('express');
    const fs = require('fs');
    const _ = require('lodash');
    const WebSocket = require('ws');

    const bodyParser = require('body-parser');

    const app = express();

    const port = process.env.PORT || 3000;
    const server = app.listen(port, function () {
        logger.info(`Process ${process.pid} : Started live at Port ${port}`);
        console.log(`Process ${process.pid} : Started live at Port ${port}`);
    });

    const wss = new WebSocket.Server({ server: server, path: "/api/flows" });

    app.use(bodyParser.json());
    app.use(require('cors')());
    app.use(function (req, res, next) {
        req.setTimeout(0);// no timeout
        res.header("Access-Control-Allow-Origin", "*");
        res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    app.use('/api', workflowRouter);
    app.use('/api', treeViewRouter);

    wss.on('connection', (ws) => {

        var incompleteList = [];
        var completedList = [];
        var RunningList = [];
        var workflowId = "";

        logger.info(`Process ${process.pid} : Established websocket connection`);
        ws.send('{"message" : "connection established"}');

        ws.on('message', async (message) => {

            let req = JSON.parse(message);
            logger.info(`Process ${process.pid} : Incoming request [ ${JSON.stringify({ req }, null, 4)}`);

            workflowId = req.workflow.workflowId;
            incompleteList = req.workflow.sequence;

            for (let step of incompleteList) {
       
                //     //Inserting to DB
                    DBHandler.updateView( {_id: workflowId, sequence: {$not: { $elemMatch: {stepId: step.stepId} } } } , {$push : {sequence: {name: step.name, stepId: step.stepId ,status: "Incomplete"} } }, (views) => {
                    console.log("Inserted incomplete step: ", step.stepId);
                });
                }

            fs.mkdir(`${process.env.ROOT_DIR}/results/${workflowId}`, async () => {

                console.log(`Running workflow...${workflowId} : ProcessID : ${process.pid}`);
                console.time(`Running time : ${workflowId}`);

                ws.send(JSON.stringify({ messageType: "status", steps: incompleteList, status: "Incomplete" }));
                
                await chainNext(Promise.resolve()) //wait for all chain to be executed

                // while (hasRunnable()) {
                //     const runnableList = getRunnables();
                //     let step = null;

                //     for (let i = 0; i < runnableList.length; i++) {
                //         step = runnableList[i];
                //         RunningList.push(step);
                //         await executeStep(step);
                //     }
                // } 

                console.timeEnd(`Running time : ${workflowId}`);
                console.log(`WorkFlow Done...${workflowId}`);
            
                ws.send(JSON.stringify({ messageType: "output" }));
            });

            async function chainNext(p) {
                console.log("Process in Chain " + process.pid);

                return p.then(async (step) => {
                    if (hasRunnable()) {
                        var listOfPs = getRunnables().map(executeStep);
                        await Promise.all(listOfPs.map(chainNext));
                    }
                })
            }

            function hasRunnable() {
                const result = _.find(incompleteList, (step) => {
                    return step.parentStepIds.length === 0 || step.parentStepIds.length === step.parentResults.length;
                });
                return !_.isEmpty(result); //return true if result is not empty
            }

            function getRunnables() {
                return _.remove(incompleteList, (step) => {
                    return step.parentStepIds.length === 0 || step.parentStepIds.length === step.parentResults.length;
                });
            }

            async function executeStep(step) {
                return new Promise((resolve, reject) => {
                    RunningList.push(step);

                    //Updating DB info
                    DBHandler.updateView( { _id: workflowId,  sequence: { $elemMatch: {stepId: step.stepId} }} , { $set: { "sequence.$.status" : "Running" } }, (views) => {
                    console.log("updated running step: ", step.stepId);
                    });


                    console.log("change status ", step.name);
                    ws.send(JSON.stringify({ messageType: "status", steps: [step], status: "Running" }));


                    step_executor(step).then(result => {
                        step.computedValue = result;

                        // propagate results for children
                        const nextStepIds = step.nextStepIds;
                        _.each(nextStepIds, (id) => {
                            const stepObj = _.find(incompleteList, (obj) => id === obj.stepId);
                            stepObj.parentResults.push(step.computedValue);
                        });
                        completedList.push(step);
                        RunningList = RunningList.filter((item) => item !== step)
                        console.log("completed ", step.name);

                        ws.send(JSON.stringify({ messageType: "status", steps: [step], status: "Completed" }));

                        //Updating DB info
                        DBHandler.updateView( { _id: workflowId,  sequence: { $elemMatch: { stepId: step.stepId } }} , { $set: { "sequence.$.status" : "Completed",  "sequence.$.computedValue" : step.computedValue}}, (views) => {
                        console.log("updated");
                        });

                        resolve(step);
                    });
                });
            };
        });
    });


}