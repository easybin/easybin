import express from "express";

import DBHandler from '../db/db-handler';

const router = express.Router();

router.use((req, res, next) => {
    next()
});

// router.get('/flow-tree', (req, res) => {
//     res.header('content-type', 'application/json');

//     TreeViewDBHandler.getAllViews((views) => {
//         res.send(views);
//     });
// });

router.get('/workflow', (req, res) => {
    res.header('content-type', 'application/json');

    console.log("In /workflow api");

    var workflow = req.param('id');
    console.log("id : ", workflow);

    // DBHandler.getAllViews((views) => {
    //     console.log("Sending workflow details");
    //     console.log("View ", views);
    //     res.send(views);
    // });

    DBHandler.getView({_id: workflow},(views) => {
        console.log("Sending workflow details");
        console.log("View ", views);
        res.send( {result : views});
    });
});





module.exports = router;