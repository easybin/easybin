import express from "express";
import * as _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import path from 'path';

import step_executor from '../workflow/step-executor';
import DBHandler from '../db/db-handler';

import Step from '../workflow/step';
import * as Task from '../workflow/tasks';
import shortid from 'shortid';
import zip from 'express-easy-zip';

import multer from "multer";
import fs from 'fs';

// const SocketServer = require("ws").Server;

var storage = multer.diskStorage(
    {
        destination: `${process.env.ROOT_DIR}/uploads/`,
        filename: function (req, file, cb) {
            cb(null, file.originalname);
        }
    }
);

var upload = multer({
    storage: storage,
}).any('value');

const router = express.Router();

router.use((req, res, next) => {
    console.log('Time:', Date.now())
    next()
});

router.use(zip());

var incompleteList = [];
var completedList = [];
var RunningList = [];

// router.use(websocketServer);
var event = require('events').EventEmitter();

router.post('/upload', async (req, res) => {
    res.header('content-type', 'application/json');

    upload(req, res, async (err) => {
        if (err) {
            console.log(err);
        }
        for (let file of req.files) {
            console.log(file);
        }
        res.status(200).send({ status: "success" });

    });
});

router.get('/files', function (req, res, next) {
    console.log("loading files");

    var filesArray = [];
    fs.readdir(`${process.env.ROOT_DIR}/uploads`, async (err, filenames) => {

        filenames.forEach(file => {
            filesArray.push({ filename: file, filepath: `${process.env.ROOT_DIR}/uploads/${file}` });
        });
        // console.log("Files : ", filesArray);
        res.status(200).send({ files: filesArray });
    });
});

router.get('/download', function (req, res, next) {
    console.log("Downloading folder");
    var dirPath = req.param('path');
    res.zip({
        files: [
            { path: dirPath, name: 'results' }
        ],
        filename: `${Date.now()}.zip`
    });
});

router.get('/source', function (req, res) {
    var filepath = req.param('path');
    // var filepath = '/home/sandani/FYP/SandBoxTester/EayBin-backend/results/test/images/image1.jpeg'
    res.sendFile(filepath);
})
// router.post('/flow', async(req, res) => {
//     res.header('content-type', 'application/json');

//     var incompleteList = [];
//     var completedList = [];
//     var RunningList = [];
//     var workflowId = "";


//     console.log("req");
//     console.log(req.body);
//     workflowId = req.body.workflow.workflowId;
//     incompleteList = req.body.workflow.sequence;
//     console.log(incompleteList)
//     for (let step of req.body.workflow.sequence) {

//     //     //Inserting to DB
//         DBHandler.updateView( {_id: workflowId, sequence: {$not: { $elemMatch: {stepId: step.stepId} } } } , {$push : {sequence: {name: step.name, stepId: step.stepId ,status: "Incomplete"} } }, (views) => {
//         console.log("Inserted incomplete step: ", step.stepId);
//     });
//     }

//     // for (let file of req.files) {
//     //     var str = file.fieldname;
//     //     var stepIndex = str.match(/workflow\[sequence\]\[(.*?)\]/i)[1];
//     //     var paramIndex = str.match(/\[params\]\[(.*?)\]/i)[1];
//     //     incompleteList[stepIndex].params[paramIndex].value = file.filename;
//     // }

//     fs.mkdir(`${process.env.ROOT_DIR}/results/${workflowId}`, async () => {

//         console.log(`Running workflow...${workflowId} : ProcessID : ${process.pid}`);
//         console.time(`Running time : ${workflowId}`);

//         if (hasRunnable()) {
//             const runnableList = getRunnables();
//             var listOfPromises = runnableList.map(executeStep);
//             await Promise.all(listOfPromises.map(chainNext));
//         }

//         console.timeEnd(`Running time : ${workflowId}`);
//         console.log(`WorkFlow Done...${workflowId}`);

//         res.status(200).send({
//             workflow: workflowId,
//             sequence: _.map(completedList, (step) => {
//                 return {
//                     name: step.name,
//                     stepId: step.stepId,
//                     computedValue: step.computedValue
//                 }
//             })
//         });
//     });


//     function chainNext(p) {
//         console.log("Inside chainnext : " + p);

//         return p.then(async (step) => {
//             if (hasRunnable()) {
//                 console.log("After ", step.name);
//                 const runnableList = getRunnables();
//                 console.log(runnableList);
//                 var listOfPs = runnableList.map(executeStep);
//                 await Promise.all(listOfPs.map(chainNext));
//             } else {
//                 return 1;
//             }
//         })
//     }

//     function hasRunnable() {
//         const result = _.find(incompleteList, (step) => {
//             return step.parentStepIds.length === 0 || step.parentStepIds.length === step.parentResults.length;
//         });
//         return !_.isEmpty(result); //return true if result is not empty
//     }

//     function getRunnables() {
//         return _.remove(incompleteList, (step) => {
//             return step.parentStepIds.length === 0 || step.parentStepIds.length === step.parentResults.length;
//         });
//     }

//     async function executeStep(step) {
//         return new Promise((resolve, reject) => {

//             RunningList.push(step);

//             //Updating DB info
//             DBHandler.updateView( { _id: workflowId,  sequence: { $elemMatch: {stepId: step.stepId} }} , { $set: { "sequence.$.status" : "Running" } }, (views) => {
//             console.log("updated running step: ", step.stepId);
//             });

//             step_executor(step).then(result => {
//                 step.computedValue = result;

//                 // propagate results for children
//                 const nextStepIds = step.nextStepIds;
//                 _.each(nextStepIds, (id) => {
//                     const stepObj = _.find(incompleteList, (obj) => id === obj.stepId);
//                     stepObj.parentResults.push(step.computedValue);
//                 });
//                 completedList.push(step);
//                 RunningList = RunningList.filter((item) => item !== step)
//                 console.log("completed ", step.name);

//                 //Updating DB info
//                 DBHandler.updateView( { _id: workflowId,  sequence: { $elemMatch: { stepId: step.stepId } }} , { $set: { "sequence.$.status" : "Completed",  "sequence.$.computedValue" : step.computedValue}}, (views) => {
//                 console.log("updated");
//                 });

//                 resolve(step);
//             });
//         });
//     };
// });

// router.get('/status', (req, res) => {
//     res.header('content-type', 'application/json');

//     console.log(`Checking Status : ProcessID : ${process.pid}`);

//     var results = [
//         {
//             status: "Completed",
//             steps: _.map(completedList, (step) => {
//                 return {
//                     name: step.name,
//                     stepId: step.stepId,
//                     output: step.computedValue
//                 }
//             })
//         },
//         {
//             status: "Running",
//             steps: _.map(RunningList, (step) => {
//                 return {
//                     name: step.name,
//                     stepId: step.stepId,
//                     output: step.computedValue
//                 }
//             })
//         },
//         {
//             status: "Incomplete",
//             steps: _.map(incompleteList, (step) => {
//                 return {
//                     name: step.name,
//                     stepId: step.stepId,
//                     output: step.computedValue
//                 }
//             })
//         }
//     ]

//     res.status(200).send({ result: results });
// });



module.exports = router;


