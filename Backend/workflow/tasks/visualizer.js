import Wrap_visualizer from '../../python-wrappers/visualizer/wrap_visualizer';

export default class Visualizer{

    constructor() {}

    execute(workflowId, stepId, ...taskParams) {
        const wrap = new Wrap_visualizer(workflowId, stepId, taskParams[0].output);
        return wrap.exec();
    }
}