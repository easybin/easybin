import Wrap_maxBin from '../../python-wrappers/maxBin/wrap_maxBin';

export default class MaxBin{

    constructor() {}

    execute(workflowId, stepId, taskParams) {
        const wrap = new Wrap_maxBin( workflowId, stepId, taskParams);
        return wrap.exec();
    }
}