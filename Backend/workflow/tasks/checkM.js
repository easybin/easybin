import Wrap_checkM from '../../python-wrappers/checkM/wrap_checkM';

export default class CheckM {

    constructor() {}

    execute(workflowId, stepId, taskParams) {
        const wrap = new Wrap_checkM(workflowId, stepId, taskParams);
        return wrap.exec();
    }
}