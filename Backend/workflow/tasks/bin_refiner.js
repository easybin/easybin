import Wrap_binRefiner from '../../python-wrappers/bin_refiner/wrap_binRefiner';

export default class BinRefiner {

    constructor() {}

    execute(workflowId, stepId,taskParams) {
        const wrap = new Wrap_binRefiner(workflowId, stepId, taskParams);
        return wrap.exec();
    }
}