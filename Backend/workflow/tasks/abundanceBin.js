import Wrap_abundanceBin from '../../python-wrappers/abundanceBin/wrap_abundanceBin';

export default class AbundanceBin {

    constructor() {}

    execute(workflowId, stepId, taskParams) {
        const wrap = new Wrap_abundanceBin( workflowId, stepId, taskParams);
        return wrap.exec();
    }
}