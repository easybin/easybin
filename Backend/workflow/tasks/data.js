export default class Data {
    data;

    constructor(data) {
        this.data = data;
    }

    //TODO if we want to do something with the input
    execute() {
        console.log("Just Data Passing Through");
        return new Promise(resolve => resolve({step: 'input', output: this.data}));
    }
}