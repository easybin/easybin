import Wrap_blast from '../../python-wrappers/blast/wrap_blast';

export class Blast {

    constructor() {}

    execute(workflowId, stepId, taskParams) {
        const wrap = new Wrap_blast( workflowId, stepId, taskParams);
        return wrap.exec();
    }
}