import Wrap_concoct from '../../python-wrappers/concoct/wrap-concoct';

export default class ConCoct {

    constructor() {}

    execute(workflowId, stepId, taskParams) {
        const wrap = new Wrap_concoct(workflowId, stepId, taskParams);
        return wrap.exec();
    }
}