import * as _ from 'lodash';

import * as Task from '../workflow/tasks';

export default function run(step) {
    switch (step.name) {
        case 'User Input':
            return (new Task.Data(step.params[0].value)).execute();
        case 'Blast':
            return (new Task.Blast()).execute( step.workflowId, step.stepId, step.params);
        case 'AbundanceBin':
            return (new Task.AbundanceBin()).execute( step.workflowId, step.stepId, step.params);
        case 'MaxBin':
            return (new Task.MaxBin()).execute( step.workflowId, step.stepId, step.params);
        case 'CONCOCT':
            return (new Task.ConCoct()).execute( step.workflowId, step.stepId, step.params);
        case 'Bin_refiner':
            return (new Task.BinRefiner()).execute( step.workflowId, step.stepId, step.parentResults);
        case 'CheckM':
            return (new Task.CheckM()).execute( step.workflowId, step.stepId, step.parentResults);
        case 'Visualizer':
            return (new Task.Visualizer()).execute( step.workflowId, step.stepId, step.parentResults);
        
    }
}
